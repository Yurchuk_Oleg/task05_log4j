package com.epam;

import com.epam.content.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Person person1 = new Person("Oleg", "Yurchuk", 19, true);
        Person person2 = new Person("Egor", "Murza", 19, true);
        Person person3 = new Person("Andriy", "Nazarchuk", 19, false);
        System.out.println(person1.toString());
        System.out.println(person2.toString());
        System.out.println(person3.toString());

        logger.error("IOException", new IOException("Test"));
        logger.info("This is info message");
        logger.warn("This is warn message");
        logger.debug("This is debug message");
        logger.error("This is error message");
        logger.fatal("This is fatal message");
    }
}
