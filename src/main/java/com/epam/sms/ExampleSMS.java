package com.epam.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC3b2ba45749847c7c6b35923449b309c2";
    public static final String AUTH_TOKEN = "53da96b9755911771c49b28c1d05aca5";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380672948987"),
                        new PhoneNumber("+14695072119"),str).create();
    }
}
